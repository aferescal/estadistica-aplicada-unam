# Estadística Aplicada (IIMAS - UNAM) <img src="images/iimas.jpg" width="65" heigh="65">

## Autor

Angel Escalante

## Descripción

Aquí subiré mis notas sobre las materias que tome durante la Especialidad en Estadística Aplicada de la UNAM. Usaré principalmente los lenguajes de programación `R` y `Julia`.
